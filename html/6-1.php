<div class="row">
	<div class="col">
		<center><h3 class="mt-3">QUIZ APP</h3></center>
	</div>
</div>
<?php
if(isset($_POST["try_again"])&&isset($_SESSION["current_index"])){
	session_destroy();
	?>
	<script>
		location.reload();
	</script>
	<?php
}
if(isset($_POST["next_question"])){
	$_SESSION["answered"] = 1;
	if(isset($_POST["answer"])){
		$answer = $_POST["answer"];
		if($answer===$_SESSION["quiz"][$_SESSION["current_index"]][3]){
			$_SESSION["score"]++;
		}
		$_SESSION["current_index"]++;
	}
	else{
		?>
		<div class="alert alert-warning" role="alert">
			Please answer the question.
		</div>
		<?php
	}
}
try {
	$db = new PDO("mysql:host=mysql-server;dbname=sample","root","secret");
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	if(!isset($_SESSION["answered"])){
		$get = $db->query("SELECT * FROM quizzes");
		$questions = $get->fetchAll(PDO::FETCH_NUM);
		$_SESSION["current_index"] = 0;
		shuffle($questions);
		$_SESSION["quiz"] = $questions;
		$_SESSION["score"] = 0;
	}
}
catch(PDOException $e){
	echo $e->getMessage();
}
?>

<div class="row">
	<form class="mt-5" method="post" action="<?= $_SERVER["PHP_SELF"] ?>">
		<?php
		if($_SESSION["current_index"]<10){
			?>
			<div class="row">
				<div class="col mb-4 text-center">
					<h1 class="fs-1"><?= ($_SESSION["current_index"]+1).". ".$_SESSION["quiz"][$_SESSION["current_index"]][1] ?></h1>
				</div>
			</div>
			<div class="row mt-5">
				<?php
				$choices = explode("@",$_SESSION["quiz"][$_SESSION["current_index"]][2]);
				?>
				<div class="col d-grid">
					<input type="radio" class="btn-check btn-lg" name="answer" id="answer1" value="<?= $choices[0] ?>">
					<label class="btn btn-outline-primary" for="answer1"><?= $choices[0] ?></label>
				</div>
				<div class="col d-grid">
					<input type="radio" class="btn-check btn-lg" name="answer" id="answer2" value="<?= $choices[1] ?>">
					<label class="btn btn-outline-primary" for="answer2"><?= $choices[1] ?></label>
				</div>
				<div class="col d-grid">
					<input type="radio" class="btn-check btn-lg" name="answer" id="answer3" value="<?= $choices[2] ?>">
					<label class="btn btn-outline-primary" for="answer3"><?= $choices[2] ?></label>
				</div>
			</div>
			<?php
		} 
		else{
			?>
			<div class="row">
				<div class="col text-center">
					<h1>Congratulations!</h1>
					<h2 class="mt-5">Your score is <?= $_SESSION["score"] ?>/10</h2>
				</div>
			</div>
			<?php
		}
		?>
		<div class="row justify-content-center mt-5">
			<div class="col-12 col-md-4 col-lg-4 d-grid">
				<?php
				if($_SESSION["current_index"]<=9){
					?>
					<button type="submit" name="next_question" class="btn btn-lg btn-success">
						<?php 
						if($_SESSION["current_index"]<9){
							echo "Next Question";
						}
						else{
							echo "Submit & Show Results";
						}
						?>
					</button>
					<?php
				}
				else{
					?>
					<button type="submit" name="try_again" class="btn btn-lg btn-danger">
						Try Again
					</button>
					<?php
				}
				?>
			</div>
		</div>
	</form>
</div>