<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>
		Excercise 6-3 - Navigation
	</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</head>
<body class="container">
	<?php
	if(!empty($_GET["page"])){
		$page = htmlentities($_GET["page"]);
		?>
		<a class="btn btn-primary mt-4" href="6-3.php">
			<i class="bi bi-arrow-return-left"></i>
			&nbsp;&nbsp;Go Back to Main Page
		</a>
		<?php
		if($page=="quiz"){
			require_once "6-1.php";
		}
		elseif ($page=="calendar") {
			require_once "6-2.php";
		}
	} 
	else{
		?>
		<div class="row justify-content-center">
			<div class="col-12 col-md-3 col-lg-3">
				<div class="card mt-5">
					<div class="card-body text-center">
						<h2 class="card-title mb-4 mt-2">Quiz App</h2>
						<a class="btn btn-primary" href="?page=quiz" class="card-link"><i class="bi bi-box-arrow-in-right"></i>&nbsp;&nbsp;&nbsp;Enter Here</a>
					</div>
				</div>
			</div>
			<div class="col-12 col-md-3 col-lg-3">
				<div class="card mt-5">
					<div class="card-body text-center">
						<h2 class="card-title mb-4 mt-2">Calendar App</h2>
						<a class="btn btn-primary" href="?page=calendar" class="card-link"><i class="bi bi-box-arrow-in-right"></i>&nbsp;&nbsp;&nbsp;Enter Here</a>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
	?>
</body>
</html>