<div class="row my-5">
	<div class="col text-center">
		<h1>Calendar APP</h1>
	</div>
</div>
<?php
if(isset($_POST['previous_btn'])){
	$date = date("Y")."-".htmlentities($_POST["month"]-1)."-01";
}
elseif (isset($_POST['next_btn'])) {
	$date = date("Y")."-".htmlentities($_POST["month"]+1)."-01";
}
else{
	$date = date("Y-m-d"); 	
}
?>
<form method="post" action="<?= $_SERVER['PHP_SELF'] ?>" class="row">
	<div class="col-2 text-center">
		<button type="submit" class="btn btn-primary" name="previous_btn" <?php echo (date('n', strtotime($date))==1)?"disabled":""; ?>>
			<i class="bi bi-chevron-left fs-4"></i>
		</button>
	</div>
	<div class="col-8 text-center">
		<select class="form-select form-select-lg" name="month" id="month">
			<option value="<?= date('n', strtotime($date)) ?>"><?= date('F', strtotime($date)) ?></option>
		</select>
	</div>
	<div class="col-2 text-center">
		<button type="submit" class="btn btn-primary" name="next_btn" <?php echo (date('n', strtotime($date))==12)?"disabled":""; ?> >
			<i class="bi bi-chevron-right fs-4"></i>
		</button>
	</div>
</form>
<div class="row justify-content-center">
	<div class="col-8">
		<table class="table table-sm table-bordered mt-3">
			<thead>
				<tr class="text-center">
					<th class="text-danger">Sun</th>
					<th>Mon</th>
					<th>Tue</th>
					<th>Wed</th>
					<th>Thu</th>
					<th>Fri</th>
					<th>Sat</th>
				</tr>
			</thead>
			<tbody class="text-center">
				<?php 

				$days = date('t', mktime(0, 0, 0, date("m", strtotime($date)), 1, date("Y", strtotime($date))));
				$row_ctr = 1;
				$total_ctr = 1;
				while ($total_ctr<=$days) {
					if($row_ctr==1){
						echo "<tr>";
						$starter_days = date("N", strtotime(date("Y", strtotime($date))."-".date("m", strtotime($date)).$total_ctr));
						$row_ctr = $row_ctr + ($starter_days-1);
						for($i=1;$i<=$starter_days;$i++){
							echo "<td></td>";
						}
					}
					if($row_ctr%7==0 && $total_ctr != $days){
						echo "</tr><tr>";
					}
					if(date("Y", strtotime($date))."-".date("m", strtotime($date))."-".$total_ctr==date("Y-m-j")){
						echo "<td class='bg-primary fw-bold text-light'>".$total_ctr."</td>";
					}
					else{
						echo "<td>".$total_ctr."</td>";
					}
					if($row_ctr%7==0 && $total_ctr == $days){
						echo "</tr>";
					}
					$row_ctr++;
					$total_ctr++;
				}
				?>
			</tbody>
		</table>
	</div>
</div>