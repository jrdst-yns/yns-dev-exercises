<script>
    const primeNum = () => {
        const max = document.getElementById("input1").value;
        let primes = "";
        for (let ctr = 2; ctr <= 100; ctr++) {
            let flag = 0;
            for (let ctr2 = 2; ctr2 < ctr; ctr2++) {
                if (ctr % ctr2 == 0) {
                    flag = 1;
                    break;
                }
            }
            if (ctr > 1 && flag == 0) {
                primes += " " + ctr.toString();
            }
        }
        document.getElementById("result").innerHTML = primes;
    }
</script>
<input type="number" name="input1" id="input1" />
<button onclick="primeNum()">Get Prime Numbers</button>
<br>
<div id="result"></div>