<html>
<head>
	<title>Exercise 2-14</title>
	<script>
		const changeImage = () => {
			const image = document.getElementById("imageSel").value;
			document.getElementById("image").src = image;
		}
	</script>
</head>
<body>
	<select onchange="changeImage();" id="imageSel">
		<option value="images/1.jpg">1</option>
		<option value="images/2.jpg">2</option>
	</select>
	<br><br>
	<img src="images/1.jpg" id="image" width="400">
</body>
</html>