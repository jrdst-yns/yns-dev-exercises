<?php
session_start();
if(isset($_POST["login"])){
    if(!empty($_POST["username"])&&!empty($_POST["password"])){
        $username = htmlentities($_POST["username"]);
        $password = $_POST["password"];
        $f = fopen("data.csv", "r");
        $fr = fread($f, filesize("data.csv"));
        fclose($f);
        $rows = explode("\n",$fr);
        $ctr = 0;
        for($i=0;$i<count($rows);$i++)
        {
            $cells = explode(",",$rows[$i]);
            if($cells[1] == $username && $cells[2]==$password) {
                $_SESSION["id"] = $cells[1];
                $ctr++;
                exit;
            } 
        }
        if($ctr>0){
            header("Location: 1-13.php");
        }
        else{
            echo "Wrong username/password!";
        }
    }
}
if(isset($_POST["logout"])){
    session_destroy();
    header("Refresh:0;");
}
if(!empty($_SESSION["id"])){
    ?>
    <form method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
        <button type="submit" name="logout">Logout</button>
    </form>
    <?php
}
else{
    ?>
    <form method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
        <label>Username:</label>
        <input type="text" name="username" id="username" />
        <br /> 
        <label>Password:</label>
        <input type="password" name="password" id="password" />
        <br />  
        <button type="submit" name="login">Login</button>
    </form>
    <?php
} 

?>