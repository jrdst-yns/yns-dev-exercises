<script>
    const mimic = () =>{
        const input = document.getElementById("input").value;
        document.getElementById("lbl").innerHTML = input;
    }
</script>
<label id='lbl' for="input"></label>
<input type="text" id="input" onkeyup="mimic()" />