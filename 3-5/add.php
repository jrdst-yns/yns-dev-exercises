<div class="container-fluid">
  <div class="row">
    <div class="col-12 col-md-3 col-lg-3">
      <?php
      if(isset($_POST["submit"])){
        if(!empty($_POST["name"])&&strlen($_POST["name"])<=200){
          if(!empty($_POST["username"])&&strlen($_POST["username"])<=20){
            if(!empty($_POST["age"])&&is_numeric($_POST["age"])){
              if(!empty($_POST["address"])){
                if(!empty($_POST["address"])){
                  $data = array();
                  $image = "";
                  $cont = 1;
                  if($_FILES["image"]["size"] > 0){
                    $target_dir = "../images/";
                    $target_file = $target_dir . basename($_FILES["image"]["name"]);
                    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
                    if (!file_exists($target_file)) {
                      if($imageFileType === "jpg" || $imageFileType === "png" || $imageFileType === "jpeg") {
                        if (!move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
                          ?>
                          <div class="alert alert-danger" role="alert">
                            There was an error in uploading.
                          </div>
                          <?php
                          $cont=0;
                        }
                        else{
                          $image = $target_file;
                        }
                      }
                      else{
                        ?>
                        <div class="alert alert-danger" role="alert">
                          Please upload an image.
                        </div>
                        <?php
                        $cont = 0;
                      }
                    }
                    else{
                      ?>
                      <div class="alert alert-danger" role="alert">
                        File already exist.
                      </div>
                      <?php
                      $cont =0;  
                    }
                  }
                  if($cont==1){
                    $username = htmlentities($_POST["username"]);
                    $name = htmlentities($_POST["name"]);
                    $address = htmlentities($_POST["address"]);
                    $password = $_POST["password"];
                    $age = $_POST["age"];
                    $cp = "None";
                    if(!empty($_POST["cp"])&&is_numeric($_POST["cp"])){
                      $cp = $_POST["cp"];
                    }
                    $add_data = $db->prepare("INSERT INTO persons(name,username,password,age,address,cp,image) VALUES(?,?,?,?,?,?,?)");
                    $add_data->bindParam(1, $name);
                    $add_data->bindParam(2, $username);
                    $add_data->bindParam(3, $password);
                    $add_data->bindParam(4, $age);
                    $add_data->bindParam(5, $address);
                    $add_data->bindParam(6, $cp);
                    $add_data->bindParam(7, $image);
                    $add_data->execute();
                    ?>
                    <table class="table table-bordered mb-4">
                      <thead>
                        <tr class="text-center">
                          <th>Name</th>
                          <th>Username</th>
                          <th>Password</th>
                          <th>Age</th>
                          <th>Address</th>
                          <th>CP</th>
                          <th>Image</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr class="text-center">
                          <td><?= $name ?></td>
                          <td><?= $username ?></td>
                          <td><?= $password ?></td>
                          <td><?= $age ?></td>
                          <td><?= $address ?></td>
                          <td><?= $cp ?></td>
                          <td><?php echo ($image!="")?"<img class='img-thumbnail' src='../".$image."' width='150' />":""; ?></td>
                        </tr>
                      </tbody>
                    </table>
                    <?php
                  }
                }
                else{
                  ?>
                  <div class="alert alert-danger" role="alert">
                    Please enter password.
                  </div>
                  <?php
                }
              }
              else{
                ?>
                <div class="alert alert-danger" role="alert">
                  Please enter address.
                </div>
                <?php
              }
            }
            else{
              ?>
              <div class="alert alert-danger" role="alert">
                Please enter valid age.
              </div>
              <?php
            }
          }
          else{
            ?>
            <div class="alert alert-danger" role="alert">
              Please enter valid username.
            </div>
            <?php
          }
        }
        else{
          ?>
          <div class="alert alert-danger" role="alert">
            please enter a valid name.
          </div>
          <?php
        }
      } 
      ?>
    </div>
  </div>
  <center>
    <a class="btn btn-lg btn-primary" href="/3-5">Go Back To Home</a>
  </center>
</div>