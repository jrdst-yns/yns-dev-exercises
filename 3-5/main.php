<div class="row my-3">
  <div class="col">
    <form method="post" action="/3-5/index.php?page=logout">
      <button name="logout" class="btn btn-danger float-end" type="submit">
        Logout
      </button>
    </form>
  </div>
</div>
<div class="row mt-2">
  <div class="col-12 col-md-3 col-lg-3">

    <form method="post" action="/3-5/index.php?page=add" enctype="multipart/form-data">
      <div class="mb-2">
        <label class="form-label"  for="name">Name:</label>
        <input class="form-control"  type="text" name="name" />
      </div>
      <div class="mb-2">
        <label class="form-label"  for="username">Username:</label>
        <input class="form-control"  type="text" name="username" />
      </div>
      <div class="mb-2">
        <label class="form-label"  for="password">Password:</label>
        <input class="form-control"  type="password" name="password" />
      </div>
      <div class="mb-2">
        <label class="form-label"  for="name">Age:</label>
        <input class="form-control"  type="number" name="age" />
      </div>
      <div class="mb-2">
        <label class="form-label"  for="name">CP:</label>
        <input class="form-control"  type="number" name="cp" />
      </div>
      <div class="mb-2">
        <label class="form-label"  for="name">Address:</label>
        <input class="form-control"  type="text" name="address" />
      </div>
      <div class="mb-2">
        <label class="form-label"  for="name">Image:</label>
        <input class="form-control"  type="file" name="image" id="image" />
      </div>
      <div class="d-grid ">
        <button class="btn btn-success mt-3" type="submit" name="submit" type="button">Submit</button>
      </div>
    </form>
  </div>
  <div class="col-12 col-md-9 col-lg-9">
    <table class='table table-bordered table-striped mb-4' >
      <thead>
        <tr class="text-center">
          <th>Name</th>
          <th>Username</th>
          <th>Password</th>
          <th>Age</th>
          <th>Address</th>
          <th>CP</th>
          <th>Image</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $active_page = 1;
        $start_index = 0;
        if(!empty($_GET["page"])&&is_numeric($_GET["page"])){
          $active_page = htmlentities($_GET["page"]);
          $start_index = ($active_page - 1) * 10;
        }
        $get_data = $db->prepare("SELECT name,username,password,age,address,cp,image FROM persons LIMIT ?,10");
        $get_data->bindParam(1,$start_index,PDO::PARAM_INT);
        $get_data->execute();
        $datas = $get_data->fetchAll();
        foreach ($datas as $data) {
          ?>
          <tr class="text-center">
            <td><?= html_entity_decode($data["name"]) ?></td>
            <td><?= html_entity_decode($data["username"]) ?></td>
            <td><?= $data["password"] ?></td>
            <td><?= $data["age"] ?></td>
            <td><?= html_entity_decode($data["address"]) ?></td>
            <td><?= $data["cp"] ?></td>
            <td><?php echo ($data["image"]!="")?"<img class='img-thumbnail' src='../".$data["image"]."' width='150' />":""; ?></td>
          </tr>
          <?php
        }
        ?>
      </tbody>
    </table>
    <?php 
    $get_no = $db->query("SELECT COUNT(id) as no_of_rows FROM persons");
    $num_rows = $get_no->fetch();
    $num_rows = ceil($num_rows["no_of_rows"]/10);
    ?> 
    <ul class="pagination">
      <?php
      for($page=1; $page <= $num_rows; $page++):
        if($page==$active_page){
          ?>
          <li class="page-item active" aria-current="page">
            <span class="page-link"><?= $page ?></span>
          </li>
          <?php
        }
        else{
          ?>
          <li class="page-item"><a class="page-link" href="?page=<?= $page ?>"><?= $page ?></a></li>
          <?php
        }
      endfor;
      ?>
    </ul>
  </div>
</div>