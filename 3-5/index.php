<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>
		Excercise 3-5
	</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</head>
<body class="container-fluid">
	<?php
	session_start();
	try {
		$db = new PDO("mysql:host=localhost;dbname=exercise","root","");
		$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	catch(PDOException $e){
		echo $e->getMessage();
	}
	if(!empty($_SESSION["id"])){
		if(!empty($_GET["page"])&&$_GET["page"]=="add"){
			require_once "add.php";
		}
		elseif (!empty($_GET["page"])&&$_GET["page"]=="logout") {
			require_once "logout.php";
		}
		else{
			require_once "main.php";
		}
	}
	else{
		require_once "login.php";
	} 

	?>
</body>
</html>