<div class="row justify-content-md-center mt-5">
	<div class="col-12 col-md-3 col-lg-3">
		<?php 
		if(isset($_POST["login"])&&!empty($_POST["username"])&&!empty($_POST["password"])){
			try{
				$db = new PDO("mysql:host=localhost;dbname=exercise","root","");
				$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				if(!empty($_POST["username"])&&!empty($_POST["password"])){
					$username = htmlentities($_POST["username"]);
					$password = $_POST["password"];
					$get = $db->prepare("SELECT username, password, id FROM persons WHERE username=? LIMIT 1");
					$get->bindParam(1, $username);
					$get->execute();
					if($get->rowCount()>0){
						$data = $get->fetch();
						if($data["password"]==$password){
							$_SESSION["id"] = $data["id"];
							header("Location:/3-5");
						}
						else{
							?>
							<div class="alert alert-danger" role="alert">
								Wrong username/password.
							</div>
							<?php
						}
					}
					else{
						?>
						<div class="alert alert-danger" role="alert">
							Wrong username/password.
						</div>
						<?php
					}
				}	
			}
			catch ( PDOException $e){
				echo $e->getMessage();
			}
		}
		?>
		<form method="post" action="<?php echo $_SERVER["PHP_SELF"]; ?>">
			<div class="mb-4">
				<label class="form-label">Username:</label>
				<input class="form-control" type="text" name="username" id="username" />
			</div>
			<div class="mb-4">
				<label class="form-label">Password:</label>
				<input class="form-control" type="password" name="password" id="password" />
			</div>
			<div class="d-grid gap-2">
				<button class="btn btn-primary" type="submit" name="login">Login</button>
			</div>
		</form>	
	</div>
</div>
