<html>
<head>
	<title>Exercise 2-12</title>
	<script>
		let start = "images/1.jpg";
		let hover = "images/2.jpg";
		const changeImage = () => {
			document.getElementById('image').src = hover;
		}
		const revertImage = () => {
			document.getElementById('image').src = start;
		}
	</script>
</head>
<body>
	<img onmouseover="changeImage();" onmouseleave="revertImage();" src="images/1.jpg" id="image" width="400">
</body>
</html>