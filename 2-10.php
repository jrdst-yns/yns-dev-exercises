<html>
<head>
	<title>Exercise 2-10</title>
	<script>
		const gotoTop = () => {
			const body = document.body;
			body.scrollTop = 0;
		}
		const gotoBottom = () => {
			const body = document.body;
			body.scrollTop = body.scrollHeight;

		}
	</script>
</head>
<body style="height: 200%;">
	<center>
		<button onclick="gotoBottom();">Go to bottom</button>
		<button style="position: absolute;top:200%;" onclick="gotoTop();">Go to top</button>
	</center>
</body>
</html>