SELECT * FROM employees WHERE last_name LIKE 'K%';

SELECT * FROM employees WHERE last_name LIKE '%i';

SELECT CONCAT(first_name," ",middle_name," ",last_name) as full_name, hire_date FROM employees WHERE hire_date BETWEEN '2015-01-01' AND '2015-03-31' ORDER BY hire_date ASC;

SELECT emp.last_name AS Employee, boss.last_name AS Boss FROM employees AS emp INNER JOIN employees AS boss ON emp.boss_id=boss.id;

SELECT emp.last_name FROM `employees` AS emp INNER JOIN departments AS dept ON emp.department_id=dept.id WHERE dept.id=3 ORDER BY emp.last_name DESC;

SELECT COUNT(middle_name) as count_has_middle FROM employees WHERE middle_name!='';

SELECT dept.name, COUNT(emp.id) AS employee_count FROM departments AS dept LEFT JOIN employees AS emp ON dept.id=emp.department_id GROUP BY dept.id HAVING employee_count >0;

SELECT CONCAT(first_name," ",middle_name," ",last_name) as full_name, hire_date FROM employees ORDER BY hire_date DESC LIMIT 1;

SELECT dept.name, COUNT(emp.id) AS employee_count FROM departments AS dept LEFT JOIN employees AS emp ON dept.id=emp.department_id GROUP BY dept.id HAVING employee_count=0;

SELECT emp.first_name,emp.middle_name,emp.last_name FROM employees AS emp LEFT JOIN employee_positions AS pos ON emp.id=pos.employee_id GROUP BY emp.id HAVING COUNT(pos.id) > 2;