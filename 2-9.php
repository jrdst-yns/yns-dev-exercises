<html>
<head>
	<title>Exercise 2-9</title>
	<script>
		const changeColor = (color, id) => {
			document.getElementById(id).style.backgroundColor = color;
		}
	</script>
</head>
<body>
	<button id='red' onclick="changeColor('red', 'red');">Red</button>
	<button id='yellow' onclick="changeColor('yellow', 'yellow');">Yellow</button>
	<button id='blue' onclick="changeColor('blue', 'blue');">Blue</button>
</body>
</html>