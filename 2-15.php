<html>
<head>
	<title>Exercise 2-15</title>
	<script>
		const updateTime = () => {
			const today = new Date();
			const date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
			const time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
			document.getElementById("time").innerHTML = date+" "+time;
		}
		setInterval(updateTime, 60000);
	</script>
</head>
<body>
	<label id="time"></label>
</body>
</html>