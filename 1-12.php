<style>
    table {
        border-collapse: collapse;
        border: 1px black solid;
    }
    tr,td, th{
        border: 1px black solid;
    }
    td, th {
        padding: 5px;
        text-align: center;
    }
</style>
<table>
    <thead>
        <tr>
            <th>Name</th>
            <th>Username</th>
            <th>Password</th>
            <th>Age</th>
            <th>Address</th>
            <th>CP</th>
            <th>Image</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $f = fopen("data.csv", "r");
        $fr = fread($f, filesize("data.csv"));
        fclose($f);
        $lines = explode("\n",$fr);
        $i = 0;
        if(!empty($_GET["page"])&&is_numeric($_GET["page"])){
            $i = ($_GET["page"] - 1) * 10;
        }
        for($row=0;$row<10&&isset($lines[$i]);$row++)
        {
            echo "<tr>";
            $cells = array(); 
            $cells = explode(",",$lines[$i]);
            for($k=0;$k<count($cells);$k++)
            {
                if($k==(count($cells)-1)){
                    echo "<td><img src='".$cells[$k]."' width='150' /></td>";
                }
                else{
                    echo "<td>".$cells[$k]."</td>";
                }
                
            }
            echo "</tr>";
            $i++;
        }
        $no_of_pages = ceil(count($lines)/10);
        echo "<br><br>";
        echo "<ul style='display: inline;'>";
        for($page = 1; $page <= $no_of_pages; $page++){
            
            if(!empty($_GET["page"])&&is_numeric($_GET["page"])){
                if($page==$_GET["page"]){
                    echo "<li>".$page."</li>";
                }
                else{
                    echo "<li><a href='1-12.php?page=".$page."'>".$page."</a></li>";
                }
            }
            else{
                if($page==1){
                    echo "<li>".$page."</li>";
                }
                else{
                    echo "<li><a href='1-12.php?page=".$page."'>".$page."</a></li>";
                }
            } 
        }
        echo "</ul>";
        ?> 
    </tbody>
</table>