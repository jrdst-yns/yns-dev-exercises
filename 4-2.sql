CREATE TABLE `therapists` ( `id` INT NOT NULL AUTO_INCREMENT , `name` VARCHAR(100) NOT NULL , PRIMARY KEY (`id`));

CREATE TABLE `daily_work_shifts` ( `id` INT NOT NULL AUTO_INCREMENT , `therapist_id` INT NOT NULL , `target_date` DATE NOT NULL , `start_time` TIME NOT NULL , `end_time` TIME NOT NULL , PRIMARY KEY (`id`));

INSERT INTO `daily_work_shifts` (`id`, `therapist_id`, `target_date`, `start_time`, `end_time`) VALUES (NULL, '1', CURDATE(), '14:00:00', '13:00:00');

INSERT INTO `daily_work_shifts` (`id`, `therapist_id`, `target_date`, `start_time`, `end_time`) VALUES (NULL, '2', CURDATE(), '22:00:00', '23:00:00');

INSERT INTO `daily_work_shifts` (`id`, `therapist_id`, `target_date`, `start_time`, `end_time`) VALUES (NULL, '3', CURDATE(), '00:00:00', '01:00:00');

INSERT INTO `daily_work_shifts` (`id`, `therapist_id`, `target_date`, `start_time`, `end_time`) VALUES (NULL, '4', CURDATE(), '05:00:00', '05:30:00');

INSERT INTO `daily_work_shifts` (`id`, `therapist_id`, `target_date`, `start_time`, `end_time`) VALUES (NULL, '1', CURDATE(), '21:00:00', '21:45:00');

INSERT INTO `daily_work_shifts` (`id`, `therapist_id`, `target_date`, `start_time`, `end_time`) VALUES (NULL, '5', CURDATE(), '05:30:00', '05:50:00');

INSERT INTO `daily_work_shifts` (`id`, `therapist_id`, `target_date`, `start_time`, `end_time`) VALUES (NULL, '3', CURDATE(), '02:00:00', '02:30:00');

SELECT therapist_id, target_date, start_time, end_time, (CASE WHEN start_time<='05:59:59' AND start_time>='00:00:00' THEN CONCAT((target_date + INTERVAL 1 DAY)," ",start_time) ELSE CONCAT(target_date," ",start_time) END) as sort_start_time FROM daily_work_shifts INNER JOIN therapists ON daily_work_shifts.therapist_id=therapists.id  
ORDER BY target_date,sort_start_time ASC;