<?php
if(isset($_POST["submit"])){
  if(!empty($_POST["name"])&&strlen($_POST["name"])<=200){
    if(!empty($_POST["username"])&&strlen($_POST["username"])<=20){
      if(!empty($_POST["age"])&&is_numeric($_POST["age"])){
        if(!empty($_POST["address"])){
          if(!empty($_POST["address"])){
            $data = array();
            $image = "";
            $cont = 1;
            if($_FILES["image"]["size"] > 0){
              $target_dir = "images/";
              $target_file = $target_dir . basename($_FILES["image"]["name"]);
              $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
              if (!file_exists($target_file)) {
                if($imageFileType === "jpg" || $imageFileType === "png" || $imageFileType === "jpeg") {
                  if (!move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
                    echo "Sorry, there was an error uploading your file.";
                    $cont=0;
                  }
                  else{
                    $image = $target_file;
                  }
                }
                else{
                  echo "Please upload an image";
                  $cont = 0;
                }
              }
              else{
                echo "File already exists!";
                $cont =0;  
              }
            }
            if($cont==1){
              $username = htmlentities($_POST["username"]);
              $name = htmlentities($_POST["name"]);
              $address = htmlentities($_POST["address"]);
              $password = $_POST["password"];
              $age = $_POST["age"];
              $cp = "None";
              if(!empty($_POST["cp"])&&is_numeric($_POST["cp"])){
                $cp = $_POST["cp"];
              }
              array_push($data,$name);
              array_push($data,$username);
              array_push($data,$password);
              array_push($data,$age);
              array_push($data,$address);
              array_push($data,$cp);
              array_push($data,$image);
              
              echo "Name: ".html_entity_decode($name)."<br>";
              echo "Username: ".$username."<br>";
              echo "Password: ".$password."<br>";
              echo "Age: ".$age."<br>";
              echo "Address: ".html_entity_decode($address)."<br>";
              echo "CP: ".$cp."<br>";

              $fp = fopen('data.csv', 'a');
              fputcsv($fp,$data);
              fclose($fp);
            }
            
          }
          else{
            echo "Please enter password";
          }
        }
        else{
          echo "Please enter address";
        }
      }
      else{
        echo "Please enter a valid age";
      }
    }
    else{
      echo "Please enter valid username.";
    }
  }
  else{
    echo "Please enter a valid name";
  }
  
} 
?>