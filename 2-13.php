<html>
<head>
	<title>Exercise 2-13</title>
	<script>
		const small = () => {
			document.getElementById('image').style.width = 200;
		}
		const medium = () => {
			document.getElementById('image').style.width = 400;
		}
		const large = () => {
			document.getElementById('image').style.width = 600;
		}
	</script>
</head>
<body>
	<img src="images/1.jpg" id="image" width="800">
	<br><br>
	<button onclick="small();">Small</button>
	<button onclick="medium();">Medium</button>
	<button onclick="large();">Large</button>
</body>
</html>